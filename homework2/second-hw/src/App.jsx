import "./App.scss";
import React, { useState, useEffect } from 'react'

import CardWrapper from "./components/CardWrapper/CardWrapper";
import Header from "./components/Header/Header";
import {getStateFromLocalStorage, saveStateToLocalStorage} from "./utils/localStorageHelper"




function App() {

	const [phones, setPhones] = useState([]);

	const [isFavorite, setIsFavorite] = useState(getStateFromLocalStorage("isFavorite") || [])
	const [isInCart, setIsInCart] = useState(getStateFromLocalStorage("isInCart") || [])


const SetToLocalStorageIsFavorite = (phone) => {

	const isSomethingInLocalStorageFav = getStateFromLocalStorage("isFavorite")

	isSomethingInLocalStorageFav ? saveStateToLocalStorage("isFavorite", [...isFavorite, phone] ) : saveStateToLocalStorage("isFavorite", [phone])
	
	setIsFavorite(getStateFromLocalStorage("isFavorite"))

}


const SetToLocalStorageIsInCart = (phone) => {
	const isSomethingInLocalStorageCart = getStateFromLocalStorage("isInCart")
	isSomethingInLocalStorageCart ? saveStateToLocalStorage("isInCart", [...isInCart, phone] ) : saveStateToLocalStorage("isInCart", [phone])
	setIsInCart(getStateFromLocalStorage("isInCart"))

	

}

	const getPhones = async () => {
	  try {
		 const res = await fetch('./iphone.json')
		 let newIphone = await res.json()
		//  console.log(newIphone)
		 setPhones(newIphone)
	  } catch(err) {
		 console.log(err)
	  }
	}

	useEffect(() => {

		getPhones();
			console.log(isFavorite)
			console.log(isInCart)
			console.log(isInCart.length);
	 }, []);

//  useEffect(() => {
// 	try {
// 		fetch('./iphone.json')
// 		.then(res => res.json())
// 		.then(res => setPhones(res)
		
// 		)
		
// 	 } catch(err) {
// 		console.log(err)
// 	 }
//  }, []);

  return (
    <div className="wrapper">
      <Header 
		lengthCart = {isInCart.length} 
		lengthFavorite = {isFavorite.length}
		/>



		<div className="cards__wrapper">
		<CardWrapper phones = {phones} testInCart = {SetToLocalStorageIsInCart} testIsFavorite = {SetToLocalStorageIsFavorite}/>
		</div>

	


    </div>
  );
}

export default App;
