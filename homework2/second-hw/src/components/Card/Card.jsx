import React, { useState, useEffect, memo}  from 'react'
import Button from '../Button/Button'
import styles from './Card.module.scss';
import Modal from "../Modal/Modal";
import PropTypes from 'prop-types';
import StarPlusOutline from '../SvgConponents/StarPlusOutline';
import StarFilled from '../SvgConponents/StarFilled';

// import { getStateFromLocalStorage, saveStateToLocalStorage } from '../../utils/localStorageHelper';


// import { CART_LS_KEY, FAVOURITES_LS_KEY } from '../../constants';

const Card = ({phone, testInCart, testIsFavorite}) => {
	

	// console.log(phone);

	const [isModalOpen, setIsModalOpen] = useState(false);

   const [isCardFavorite, setIsCardFavorite] = useState(false)




	const changeStar = ()  => {
		setIsCardFavorite(!isCardFavorite)

		
	}

	const openModal = () => {
		setIsModalOpen(true);
	 };
	 const closeModal = () => {
		setIsModalOpen(false);
	 };

  
  return (
	 <div className={styles.card} >
		<button className={styles.card__star} onClick={()=> {testIsFavorite(phone); changeStar()}}>{isCardFavorite ? <StarFilled/> : <StarPlusOutline/> }</button>
	
		<img width={250} className={styles.card__img} src={phone.imgURL} alt="iphone" />
		<p className={styles.card__name}>{phone.name}</p>
		{/* <p className={styles.card__color}>Color - {phone.color}</p> */}
		
		<div className={styles.card__info}>
	
			
			<p className={styles.card__color}>Color - <span >{phone.color}</span></p>
			<div className={styles.info__article}>
				
				<span className={styles.phone__article}>Article: </span>
				<span className={styles.article__number}>{phone.article}</span>
			</div>
		</div>
		<p className={styles.info__price}>$ {phone.price}</p>
		<Button className={styles.addbtn} text="Add to cart" onClick={openModal}>
      </Button>

				  {isModalOpen && (
        <Modal
          modalIsOpen={isModalOpen}
          setModalIsOpen={setIsModalOpen}
          closeModal={closeModal}
          closeBtn={true}
        
          text={
            "Do you want to add the phone to cart?"
          }
       
          actions={
            <>
              <Button className="butnOK" backgroundColor="#ffffff" text="Ok" onClick={() => {testInCart(phone); closeModal()}}>
                Ok
              </Button>
              <Button
                className="butnCancel"
                backgroundColor="#ffffff"
					//  onClick={()=>{handleAddToCart(productForCart); closeModal()}}
                onClick={closeModal}
                text="Cancel"
              >
                Cancel
              </Button>
            </>
          }
        />
      )}
	 </div>
  )
}

Card.propTypes = {
	name: PropTypes.string.isRequired,
	price: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number,
	 ]),
	imgURL: PropTypes.string.isRequired,
	article:PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number,
	 ]),
	color: PropTypes.string,

 }
 
 Card.defaultProps = {
	name: "",
	price: "",
	imgURL: "",
	article: "",
	color: "",
 }

export default memo (Card)