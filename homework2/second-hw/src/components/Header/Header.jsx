import React, { useEffect, useState, memo } from 'react';
import IconCart from '../SvgConponents/IconCart';
import StarFilled from '../SvgConponents/StarFilled';
import styles from './Header.module.scss';
import PropTypes from 'prop-types';


const Header = ({ cartItems , lengthCart, lengthFavorite}) => {

	const [cartAmount, setCartAmount] = useState(0);

	// 

	return (
		<>
			<header className={styles.header} >
				<p className={styles.header__logo} >iShop</p>

				<ul className={styles.navigation__list} >
					<li className={styles.navigation__item} ><button className={styles.navigation__link} href=""> {lengthFavorite} <StarFilled /></button></li>
					<li className={styles.navigation__item} ><button className={styles.navigation__link} href="">   <IconCart lengthCart={lengthCart}/> </button></li>
				</ul>
			</header>
		</>
	)
}

// Header.protoTypes = {
// 	cartProducts: PropTypes.array, 
// 	favourites: PropTypes.array,
// }

// Header.defaultProps = {
// 	cartProducts: [],
// 	favourites: [],
// }

export default memo(Header);



