import React , { memo }  from 'react'
import Card from '../Card/Card'
import PropTypes from 'prop-types';



const CardWrapper = ({phones, testInCart, testIsFavorite}) => {
	// console.log(phones);
  return (
	 <>
	{phones && phones.map((phone, index) => (

		<Card phone = {phone} key = {index} testInCart ={testInCart} testIsFavorite = {testIsFavorite} />


	) )} 


	 </>
  )
}

CardWrapper.propTypes = {
	phones: PropTypes.array

 }
 
 CardWrapper.defaultProps = {
	phones: []
}

export default memo (CardWrapper)