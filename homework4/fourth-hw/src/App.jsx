import "./App.scss";
import React, { useEffect } from "react";

import Header from "./components/Header/Header";

import { useDispatch } from "react-redux";
import { fetchItems } from "./redux/items/actionCreators";
import { fetchFavourite } from "./redux/favourite/actionCreators";
import AppRoutes from "./AppRoutes";
import { fetchCart } from "./redux/cart/actionCreators";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchItems());
    dispatch(fetchFavourite());
    dispatch(fetchCart());
  }, []);

  return (
    <div className="wrapper">
      <Header />
      <AppRoutes />
    </div>
  );
}

export default App;
