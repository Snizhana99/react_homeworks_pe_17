import React from "react";
import CardWrapper from "../../components/CardWrapper/CardWrapper";
import { useSelector } from "react-redux";
const CartPage = ({
  addPhoneInCart,
  addPhoneInFavourite,
  deleteFromFavorites,
  deleteFromCart,
}) => {
  const cart = useSelector((state) => state.cart.items);
  return (
    <div>
      CartPage
      <div className="cards__wrapper">
        <CardWrapper
          phones={cart}
          addPhoneInCart={addPhoneInCart}
          addPhoneInFavourite={addPhoneInFavourite}
          deleteFromFavorites={deleteFromFavorites}
          deleteFromCart={deleteFromCart}
        />
      </div>
    </div>
  );
};

export default CartPage;
