import { useSelector } from "react-redux";

import CardWrapper from "../../components/CardWrapper/CardWrapper";

const FavoritePage = ({
  addPhoneInCart,
  addPhoneInFavourite,
  deleteFromFavorites,
  deleteFromCart,
}) => {
  const favorites = useSelector((state) => state.favourite.items);
  console.log(favorites);
  return (
    <div>
      FavoritePage
      <div className="cards__wrapper">
        <CardWrapper
          phones={favorites}
          addPhoneInCart={addPhoneInCart}
          addPhoneInFavourite={addPhoneInFavourite}
          deleteFromFavorites={deleteFromFavorites}
          deleteFromCart={deleteFromCart}
        />
      </div>
    </div>
  );
};

export default FavoritePage;
