import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import CardWrapper from "../../components/CardWrapper/CardWrapper";

const HomePage = (props) => {
  const phones = useSelector((state) => state.items.items);

  const {
    addPhoneInCart,
    addPhoneInFavourite,
    deleteFromFavorites,
    deleteFromCart,
  } = props;

  return (
    <>
      <div className="cards__wrapper">
        <CardWrapper
          phones={phones}
          addPhoneInCart={addPhoneInCart}
          addPhoneInFavourite={addPhoneInFavourite}
          deleteFromFavorites={deleteFromFavorites}
          deleteFromCart={deleteFromCart}
        />
      </div>
    </>
  );
};

export default HomePage;
