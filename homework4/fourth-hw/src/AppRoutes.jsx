import React from "react";
import { Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import FavoritePage from "./pages/FavoritePage/FavoritePage";
import PropTypes from "prop-types";
import CartPages from "./pages/CartPage/CartPage";

const AppRoutes = ({
  phones,
  favorites,
  cart,
  addPhoneInCart,
  addPhoneInFavourite,
  deleteFromFavorites,
  deleteFromCart,
}) => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <HomePage
            phones={phones}
            addPhoneInCart={addPhoneInCart}
            addPhoneInFavourite={addPhoneInFavourite}
            deleteFromFavorites={deleteFromFavorites}
            deleteFromCart={deleteFromCart}
          />
        }
      />
      <Route
        path="/favorites"
        element={
          <FavoritePage
            favorites={favorites}
            addPhoneInCart={addPhoneInCart}
            addPhoneInFavourite={addPhoneInFavourite}
            deleteFromFavorites={deleteFromFavorites}
            deleteFromCart={deleteFromCart}
          />
        }
      />
      <Route
        path="/cart"
        element={
          <CartPages
            cart={cart}
            addPhoneInCart={addPhoneInCart}
            addPhoneInFavourite={addPhoneInFavourite}
            deleteFromFavorites={deleteFromFavorites}
            deleteFromCart={deleteFromCart}
          />
        }
      />
    </Routes>
  );
};

AppRoutes.propTypes = {
  phones: PropTypes.arrayOf(PropTypes.object),
  favorites: PropTypes.arrayOf(PropTypes.object),
  cart: PropTypes.arrayOf(PropTypes.object),
  addPhoneInCart: PropTypes.func,
  addPhoneInFavourite: PropTypes.func,
}.isRequired;

AppRoutes.defaultProps = {};

export default AppRoutes;
