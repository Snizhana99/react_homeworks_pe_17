import { SET_FAVOURITE, ADD_FAVOURITE, DELETE_FAVOURITE } from "./actions";
import { saveStateToLocalStorage } from "../../utils/localStorageHelper";
const initialValue = {
  items: [],
};

const favouriteReducer = (state = initialValue, action) => {
  switch (action.type) {
    case SET_FAVOURITE: {
      return { ...state, items: action.payload };
    }

    case ADD_FAVOURITE: {
      const newItems = [...state.items];
      const item = action.payload;
      newItems.push(item);
      saveStateToLocalStorage("favorites", newItems);
      return { ...state, items: newItems };
    }

    case DELETE_FAVOURITE: {
      const newItems = [...state.items];
      const id = action.payload;
      const index = newItems.findIndex((el) => el.id === id);

      newItems.splice(index, 1);
      saveStateToLocalStorage("favorites", newItems);
      return { ...state, items: newItems };
    }

    default:
      return state;
  }
};

export default favouriteReducer;
