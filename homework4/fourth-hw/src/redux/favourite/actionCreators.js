import { SET_FAVOURITE, ADD_FAVOURITE, DELETE_FAVOURITE } from "./actions";
import { getStateFromLocalStorage } from "../../utils/localStorageHelper";

export const fetchFavourite = () => {
  return async (dispatch) => {
    try {
      const data = getStateFromLocalStorage("favorites");
      dispatch(setFavourite(data));
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
};

export const setFavourite = (items) => ({
  type: SET_FAVOURITE,
  payload: items,
});

export const addFavourite = (item) => ({
  type: ADD_FAVOURITE,
  payload: item,
});
export const deleteFavourite = (id) => ({
  type: DELETE_FAVOURITE,
  payload: id,
});
