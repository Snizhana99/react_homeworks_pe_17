import React, { useState, useEffect } from "react";
import CardWrapper from "../../components/CardWrapper/CardWrapper";
import {
  getStateFromLocalStorage,
  saveStateToLocalStorage,
} from "../../utils/localStorageHelper";

const HomePage = (props) => {
  const {
    phones,
    addPhoneInCart,
    addPhoneInFavourite,
    deleteFromFavorites,
    deleteFromCart,
  } = props;

  return (
    <>
      <div className="cards__wrapper">
        <CardWrapper
          phones={phones}
          addPhoneInCart={addPhoneInCart}
          addPhoneInFavourite={addPhoneInFavourite}
          deleteFromFavorites={deleteFromFavorites}
          deleteFromCart={deleteFromCart}
        />
      </div>
    </>
  );
};

export default HomePage;
