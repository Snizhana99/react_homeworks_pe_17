import { useEffect, useState } from "react";
import Card from "../../components/Card/Card";
import CardWrapper from "../../components/CardWrapper/CardWrapper";
import {
  getStateFromLocalStorage,
  saveStateToLocalStorage,
} from "../../utils/localStorageHelper";

const FavoritePage = ({
  favorites,
  addPhoneInCart,
  addPhoneInFavourite,
  deleteFromFavorites,
  deleteFromCart,
}) => {
  console.log(favorites);
  return (
    <div>
      FavoritePage
      <div className="cards__wrapper">
        <CardWrapper
          phones={favorites}
          addPhoneInCart={addPhoneInCart}
          addPhoneInFavourite={addPhoneInFavourite}
          deleteFromFavorites={deleteFromFavorites}
          deleteFromCart={deleteFromCart}
        />
      </div>
    </div>
  );
};

export default FavoritePage;
