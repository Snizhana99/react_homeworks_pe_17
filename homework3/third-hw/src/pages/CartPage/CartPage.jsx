import React from "react";
import CardWrapper from "../../components/CardWrapper/CardWrapper";

const CartPage = ({
  cart,
  addPhoneInCart,
  addPhoneInFavourite,
  deleteFromFavorites,
  deleteFromCart,
}) => {
  return (
    <div>
      CartPage
      <div className="cards__wrapper">
        <CardWrapper
          phones={cart}
          addPhoneInCart={addPhoneInCart}
          addPhoneInFavourite={addPhoneInFavourite}
          deleteFromFavorites={deleteFromFavorites}
          deleteFromCart={deleteFromCart}
        />
      </div>
    </div>
  );
};

export default CartPage;
