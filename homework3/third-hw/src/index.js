import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./App.scss";
import { ErrorBoundary } from "react-error-boundary";
import { BrowserRouter } from "react-router-dom";
import SomethingWentWrong from "./components/SomethingWentWrong/SomethingWentWrong";
// ReactDOM.render(<App />, document.getElementById("root"));
const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <ErrorBoundary fallback={<SomethingWentWrong />}>
    <BrowserRouter>
      {/* <React.StrictMode> */}
      <App />
      {/* </React.StrictMode> */}
    </BrowserRouter>
  </ErrorBoundary>
);
