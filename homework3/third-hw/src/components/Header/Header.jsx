import React, { useEffect, useState, memo } from "react";
import IconCart from "../SvgConponents/IconCart";
import StarFilled from "../SvgConponents/StarFilled";
import styles from "./Header.module.scss";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";

const Header = ({ lengthCart, lengthFavorite }) => {
  return (
    <>
      <header className={styles.header}>
        <nav className={styles.header__navigation}>
          <ul className={styles.navigation__list}>
            <li className={styles.navigation__item}>
              <NavLink className={styles.header__logo} to="/">
                iShop
              </NavLink>
            </li>
            <li className={styles.navigation__item}>
              <NavLink className={styles.navigation__link} to="/favorites">
                {lengthFavorite} <StarFilled />
              </NavLink>
            </li>
            <li className={styles.navigation__item}>
              <NavLink className={styles.navigation__link} to="/cart">
                <IconCart lengthCart={lengthCart} />
              </NavLink>
            </li>
          </ul>
        </nav>
      </header>
    </>
  );
};

// Header.protoTypes = {
// 	cartProducts: PropTypes.array,
// 	favourites: PropTypes.array,
// }

// Header.defaultProps = {
// 	cartProducts: [],
// 	favourites: [],
// }

export default memo(Header);
