import React, { useState, useEffect, memo } from "react";
import Button from "../Button/Button";
import styles from "./Card.module.scss";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";
import StarPlusOutline from "../SvgConponents/StarPlusOutline";
import StarFilled from "../SvgConponents/StarFilled";

import {
  getStateFromLocalStorage,
  saveStateToLocalStorage,
} from "../../utils/localStorageHelper";

// import { CART_LS_KEY, FAVOURITES_LS_KEY } from '../../constants';

const Card = ({
  phone,
  addPhoneInCart,
  addPhoneInFavourite,
  deleteFromFavorites,
  deleteFromCart,
}) => {
  // console.log(phone);

  const [isModalOpen, setIsModalOpen] = useState(false);

  const [isCardFavorite, setIsCardFavorite] = useState(false);
  const [isCardInCart, setIsCardInCart] = useState(false);

  //   const changeStar = () => {
  //     setIsCardFavorite(!isCardFavorite);
  //   };

  useEffect(() => {
    const favorites = getStateFromLocalStorage("favorites") || [];
    const cart = getStateFromLocalStorage("cart") || [];

    const isInFavorite = !!favorites.find(
      (product) => product.article === phone.article
    );

    const isInCart = !!cart.find(
      (product) => product.article === phone.article
    );

    setIsCardFavorite(isInFavorite);
    setIsCardInCart(isInCart);
  }, [phone]); // якщо були якість зміні в залежності [phone] тоді запускається useEffect

  const handleFavorite = () => {
    if (isCardFavorite) {
      deleteFromFavorites(phone.article);
      setIsCardFavorite(false);
    } else {
      addPhoneInFavourite(phone);
      setIsCardFavorite(true);
    }
  };

  const handleCart = () => {
    if (isCardInCart) {
      deleteFromCart(phone.article);
      setIsCardInCart(false);
    } else {
      addPhoneInCart(phone);
      setIsCardInCart(true);
    }
  };

  const openModal = () => {
    setIsModalOpen(true);
  };
  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div className={styles.card}>
      <button className={styles.card__star} onClick={handleFavorite}>
        {isCardFavorite ? <StarFilled /> : <StarPlusOutline />}
      </button>

      <img
        width={250}
        className={styles.card__img}
        src={phone.imgURL}
        alt="iphone"
      />
      <p className={styles.card__name}>{phone.name}</p>

      <div className={styles.card__info}>
        <p className={styles.card__color}>
          Color - <span>{phone.color}</span>
        </p>
        <div className={styles.info__article}>
          <span className={styles.phone__article}>Article: </span>
          <span className={styles.article__number}>{phone.article}</span>
        </div>
      </div>
      <p className={styles.info__price}>$ {phone.price}</p>
      <Button
        className={styles.addbtn}
        text={isCardInCart ? "Delete from cart" : "Add to cart"}
        onClick={openModal}
      ></Button>

      {isModalOpen && (
        <Modal
          modalIsOpen={isModalOpen}
          setModalIsOpen={setIsModalOpen}
          closeModal={closeModal}
          closeBtn={true}
          text={
            isCardInCart
              ? "Do you want to delete the phone from cart?"
              : "Do you want to add the phone to cart?"
          }
          actions={
            <>
              <Button
                className="butnOK"
                backgroundColor="#ffffff"
                text="Ok"
                onClick={() => {
                  handleCart();
                  closeModal();
                }}
              >
                Ok
              </Button>
              <Button
                className="butnCancel"
                backgroundColor="#ffffff"
                //  onClick={()=>{handleAddToCart(productForCart); closeModal()}}
                onClick={closeModal}
                text="Cancel"
              >
                Cancel
              </Button>
            </>
          }
        />
      )}
    </div>
  );
};

// Card.propTypes = {
// 	name: PropTypes.string.isRequired,
// 	price: PropTypes.oneOfType([
// 		PropTypes.string,
// 		PropTypes.number,
// 	 ]),
// 	imgURL: PropTypes.string.isRequired,
// 	article:PropTypes.oneOfType([
// 		PropTypes.string,
// 		PropTypes.number,
// 	 ]),
// 	color: PropTypes.string,

//  }

//  Card.defaultProps = {
// 	name: "",
// 	price: "",
// 	imgURL: "",
// 	article: "",
// 	color: "",
//  }

Card.propTypes = {
  phone: PropTypes.object,
  addPhoneInCart: PropTypes.func,
  addPhoneInFavourite: PropTypes.func,
  name: PropTypes.string.isRequired,
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  imgURL: PropTypes.string.isRequired,
  article: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  color: PropTypes.string,
};

Card.defaultProps = {
  // 	phone: [],
  // addPhoneInCart:(),
  // addPhoneInFavourite:(),
  // console.log(phone);
  name: "",
  price: "",
  imgURL: "",
  article: "",
  color: "",
};

export default memo(Card);
