import React, { memo } from "react";
import Card from "../Card/Card";
import PropTypes from "prop-types";

const CardWrapper = ({
  phones,
  addPhoneInCart,
  addPhoneInFavourite,
  deleteFromFavorites,
  deleteFromCart,
}) => {
  // console.log(phones);
  return (
    <>
      {phones &&
        phones.map((phone, index) => (
          //key={index} - додається на обгорту всіх продуктів(карток)
          <div className="cards__wrapper" key={index}>
            <Card
              phone={phone}
              addPhoneInCart={addPhoneInCart}
              addPhoneInFavourite={addPhoneInFavourite}
              deleteFromFavorites={deleteFromFavorites}
              deleteFromCart={deleteFromCart}
            />
          </div>
        ))}
    </>
  );
};

CardWrapper.propTypes = {
  phones: PropTypes.array,
};

CardWrapper.defaultProps = {
  phones: [],
};

export default CardWrapper;
