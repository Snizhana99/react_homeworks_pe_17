import "./App.scss";
import React, { useState, useEffect } from "react";

import CardWrapper from "./components/CardWrapper/CardWrapper";
import Header from "./components/Header/Header";
import {
  getStateFromLocalStorage,
  saveStateToLocalStorage,
} from "./utils/localStorageHelper";
import AppRoutes from "./AppRoutes";

function App() {
  const [phones, setPhones] = useState([]);

  const [favorites, setFavorites] = useState(
    getStateFromLocalStorage("favorites") || []
  );
  const [cart, setCart] = useState(getStateFromLocalStorage("cart") || []);

  const addPhoneInFavourite = (phone) => {
    const isInFavorite = !!favorites.find(
      (product) => product.article === phone.article
    );
    if (isInFavorite) {
      return;
    }
    const newFavorites = [...favorites, phone];
    saveStateToLocalStorage("favorites", newFavorites);
    setFavorites(newFavorites);
  };

  const deleteFromFavorites = (article) => {
    const newFavorites = favorites.filter(
      (product) => product.article !== article // не дорівнює - правдиве значення \ всі телефони окрім того артиклю якого ми передаємо
    );

    saveStateToLocalStorage("favorites", newFavorites);

    setFavorites(newFavorites);
  };

  const addPhoneInCart = (phone) => {
    const newCart = [...cart, phone];

    saveStateToLocalStorage("cart", newCart);
    setCart(newCart);
  };

  const deleteFromCart = (article) => {
    const newCart = cart.filter(
      (product) => product.article !== article // не дорівнює - правдиве значення \ всі телефони окрім того артиклю якого ми передаємо
    );

    saveStateToLocalStorage("cart", newCart);

    setCart(newCart);
  };

  const getPhones = async () => {
    try {
      const res = await fetch("./iphone.json");
      let newIphone = await res.json();
      //  console.log(newIphone)
      setPhones(newIphone);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getPhones();
    console.log(favorites);
    console.log(cart);
    console.log(cart.length);
  }, []);

  //  useEffect(() => {
  // 	try {
  // 		fetch('./iphone.json')
  // 		.then(res => res.json())
  // 		.then(res => setPhones(res)

  // 		)

  // 	 } catch(err) {
  // 		console.log(err)
  // 	 }
  //  }, []);

  return (
    <div className="wrapper">
      <Header lengthCart={cart.length} lengthFavorite={favorites.length} />
      <AppRoutes
        phones={phones}
        favorites={favorites}
        cart={cart}
        addPhoneInCart={addPhoneInCart}
        addPhoneInFavourite={addPhoneInFavourite}
        deleteFromFavorites={deleteFromFavorites}
        deleteFromCart={deleteFromCart}
      />

      {/* <div className="cards__wrapper">
		<CardWrapper phones = {phones} testInCart = {SetToLocalStoragecart} testIsFavorite = {SetToLocalStorageIsFavorite}/>
		</div> */}
    </div>
  );
}

export default App;
