import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store";
import { DisplayModeProvider } from "./DisplayModeContext"; // Припустимо, що це шлях до вашого контексту

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <DisplayModeProvider>
        <App />
      </DisplayModeProvider>
    </BrowserRouter>
  </Provider>
);
