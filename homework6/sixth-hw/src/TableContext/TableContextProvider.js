import React, { useState } from "react";
import TableContext from ".";
import Card from "../components/Card/Card";
import DataTable from "../components/Table/Table";

function TableContextProvider({ children }) {
  const [theme, setTheme] = useState(<Card />);

  const toggleTheme = () => {
    setTheme((prev) => (prev === <Card /> ? <DataTable /> : <Card />));
    console.log("Table");
  };

  const value = {
    theme,
    toggleTheme,
  };

  return (
    <TableContext.Provider value={value}>{children}</TableContext.Provider>
  );
}

export default TableContextProvider;
