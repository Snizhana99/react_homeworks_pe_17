import React from "react";

const StarFilled = () => {
  return (
    <svg
      width="30"
      height="30"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_2_288)">
        <path
          d="M8 11.5133L12.12 14L11.0267 9.31334L14.6667 6.16L9.87333 5.74667L8 1.33334L6.12667 5.74667L1.33333 6.16L4.96667 9.31334L3.88 14L8 11.5133Z"
          fill="#FFAA2C"
        />
      </g>
      <defs>
        <clipPath id="clip0_2_288">
          <rect width="16" height="16" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

export default StarFilled;
