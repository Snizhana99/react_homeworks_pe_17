import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Button from "./Button";

describe("Button component", () => {
  it("renders without crashing", () => {
    render(<Button />);
  });

  it("renders with correct text and id", () => {
    const buttonText = "Click me";
    const buttonId = "test-button";
    render(<Button text={buttonText} id={buttonId} />);
    const buttonElement = screen.getByRole("button");
    expect(buttonElement).toBeInTheDocument();
    //  expect(buttonElement).toHaveAttribute("id", buttonId);
  });

  it("calls onClick when clicked", () => {
    const mockOnClick = jest.fn();
    render(<Button onClick={mockOnClick} />);
    const buttonElement = screen.getByRole("button");
    fireEvent.click(buttonElement);
    expect(mockOnClick).toHaveBeenCalled();
  });

  it("has a custom background color", () => {
    const backgroundColor = "blue";
    render(<Button backgroundColor={backgroundColor} />);
    const buttonElement = screen.getByRole("button");
    expect(buttonElement).toHaveStyle(`background-color: ${backgroundColor}`);
  });

  it("applies custom className", () => {
    const customClassName = "custom-button";
    render(<Button className={customClassName} />);
    const buttonElement = screen.getByRole("button");
    expect(buttonElement).toHaveClass(customClassName);
  });
});
test("Button component matches snapshot", () => {
  const { asFragment } = render(
    <Button
      id="my-button"
      className="custom-button"
      backgroundColor="#FF0000"
      text="Click me"
      onClick={() => {}}
    />
  );

  // Создаем снимок компонента
  expect(asFragment()).toMatchSnapshot();
});
