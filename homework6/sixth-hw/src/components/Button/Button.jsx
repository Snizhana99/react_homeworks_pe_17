import React from "react";
import "./Button.module.scss";

const Button = ({ id, className, backgroundColor, text, onClick }) => {
  return (
    <button className={className} style={{ backgroundColor }} onClick={onClick}>
      {text} {id}
    </button>
  );
};

export default Button;
