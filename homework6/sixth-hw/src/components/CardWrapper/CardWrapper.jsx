import React from "react";
import Card from "../Card/Card";
import PropTypes from "prop-types";

const CardWrapper = ({
  phones,
  addPhoneInCart,
  addPhoneInFavourite,
  deleteFromFavorites,
  deleteFromCart,
}) => {
  // console.log(phones);
  return (
    <div className="cards__wrapper">
      {phones &&
        phones.map((phone, index) => (
          //key={index} - додається на обгорту всіх продуктів(карток)
          //  <div className="cards__wrapper" key={index}>
          <Card
            key={index}
            phone={phone}
            addPhoneInCart={addPhoneInCart}
            addPhoneInFavourite={addPhoneInFavourite}
            deleteFromFavorites={deleteFromFavorites}
            deleteFromCart={deleteFromCart}
          />
          //  </div>
        ))}
    </div>
  );
};

CardWrapper.propTypes = {
  phones: PropTypes.array,
};

CardWrapper.defaultProps = {
  phones: [],
};

export default CardWrapper;
