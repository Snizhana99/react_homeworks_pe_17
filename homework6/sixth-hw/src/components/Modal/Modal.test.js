import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import Modal from "./Modal.jsx";
// import Button from "../Button/Button";
import "@testing-library/jest-dom/extend-expect";

describe("Modal component", () => {
  const closeModalMock = jest.fn();

  const modalProps = {
    id: "test-modal",
    closeBtn: true,
    //  actions: <Button>Confirm</Button>,
    text: "Test modal content",
    title: "Test Modal",
    closeModal: closeModalMock,
  };

  it("renders correctly", () => {
    render(<Modal {...modalProps} />);

    expect(screen.getByText(modalProps.title)).toBeInTheDocument();
    expect(screen.getByText(modalProps.text)).toBeInTheDocument();
    expect(screen.getByText("✕")).toBeInTheDocument();
    expect(screen.getByText("Test Modal")).toBeInTheDocument();
  });

  it("closes when close button is clicked", () => {
    render(<Modal {...modalProps} />);
    const closeBtn = screen.getByText("✕");
    fireEvent.click(closeBtn);

    expect(closeModalMock).toHaveBeenCalledTimes(1);
  });

  it("does not close when modal content is clicked", () => {
    render(<Modal {...modalProps} />);
    const modalContent = screen.getByTestId("modal-content");
    fireEvent.click(modalContent);

    expect(closeModalMock).not.toHaveBeenCalled();
  });
});
