import React, { useState, memo } from "react";
import Button from "../Button/Button";
import styles from "./Card.module.scss";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";
import StarPlusOutline from "../SvgConponents/StarPlusOutline";
import StarFilled from "../SvgConponents/StarFilled";

import { useDispatch, useSelector } from "react-redux";
import {
  addFavourite,
  deleteFavourite,
} from "../../redux/favourite/actionCreators";
import { addCart, deleteCart } from "../../redux/cart/actionCreators";

const Card = ({ phone }) => {
  // console.log(phone);
  const dispatch = useDispatch();

  const [isModalOpen, setIsModalOpen] = useState(false);

  const favouriteCard = useSelector((state) =>
    state.favourite.items.find((item) => item.id === phone.id)
  );
  const isCardFavorite = !!favouriteCard;

  const cartCard = useSelector((state) =>
    state.cart.items.find((item) => item.id === phone.id)
  );

  const isCardInCart = !!cartCard;

  const handleFavorite = () => {
    if (isCardFavorite) {
      dispatch(deleteFavourite(phone.id));
    } else {
      dispatch(addFavourite(phone));
    }
  };

  const handleCart = () => {
    if (isCardInCart) {
      dispatch(deleteCart(phone.id));
    } else {
      dispatch(addCart(phone));
    }
  };

  const openModal = () => {
    setIsModalOpen(true);
  };
  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div className={styles.card}>
      <button className={styles.card__star} onClick={handleFavorite}>
        {isCardFavorite ? <StarFilled /> : <StarPlusOutline />}
      </button>

      <img
        width={250}
        className={styles.card__img}
        src={phone.imgURL}
        alt="iphone"
      />
      <p className={styles.card__name}>{phone.name}</p>

      <div className={styles.card__info}>
        <p className={styles.card__color}>
          Color - <span>{phone.color}</span>
        </p>
        <div className={styles.info__article}>
          <span className={styles.phone__article}>Article: </span>
          <span className={styles.article__number}>{phone.article}</span>
        </div>
      </div>
      <p className={styles.info__price}>$ {phone.price}</p>
      <Button
        className={styles.addbtn}
        text={isCardInCart ? "Delete from cart" : "Add to cart"}
        onClick={openModal}
      ></Button>

      {isModalOpen && (
        <Modal
          modalIsOpen={isModalOpen}
          setModalIsOpen={setIsModalOpen}
          closeModal={closeModal}
          closeBtn={true}
          text={
            isCardInCart
              ? "Do you want to delete the phone from cart?"
              : "Do you want to add the phone to cart?"
          }
          actions={
            <>
              <Button
                className="butnOK"
                backgroundColor="#ffffff"
                text="Ok"
                onClick={() => {
                  handleCart();
                  closeModal();
                }}
              >
                Ok
              </Button>
              <Button
                className="butnCancel"
                backgroundColor="#ffffff"
                //  onClick={()=>{handleAddToCart(productForCart); closeModal()}}
                onClick={closeModal}
                text="Cancel"
              >
                Cancel
              </Button>
            </>
          }
        />
      )}
    </div>
  );
};

Card.propTypes = {
  phone: PropTypes.object,
  addPhoneInCart: PropTypes.func,
  addPhoneInFavourite: PropTypes.func,
  name: PropTypes.string.isRequired,
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  imgURL: PropTypes.string.isRequired,
  article: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  color: PropTypes.string,
};

Card.defaultProps = {
  // 	phone: [],
  // addPhoneInCart:(),
  // addPhoneInFavourite:(),
  // console.log(phone);
  name: "",
  price: "",
  imgURL: "",
  article: "",
  color: "",
};

export default memo(Card);
