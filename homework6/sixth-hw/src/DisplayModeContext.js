import React, { createContext, useState, useContext } from "react";

const DisplayModeContext = createContext();

export const useDisplayMode = () => {
  return useContext(DisplayModeContext);
};

export const DisplayModeProvider = ({ children }) => {
  const [displayMode, setDisplayMode] = useState("grid"); // Початковий режим - "grid" (картки)

  const toggleDisplayMode = () => {
    setDisplayMode((prevMode) => (prevMode === "grid" ? "table" : "grid"));
  };

  return (
    <DisplayModeContext.Provider value={{ displayMode, toggleDisplayMode }}>
      {children}
    </DisplayModeContext.Provider>
  );
};
