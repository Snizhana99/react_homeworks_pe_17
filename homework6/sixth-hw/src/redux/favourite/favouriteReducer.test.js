import favouriteReducer from "./reducer";
import { SET_FAVOURITE, ADD_FAVOURITE, DELETE_FAVOURITE } from "./actions";

describe("favouriteReducer", () => {
  it("should return the initial state", () => {
    expect(favouriteReducer(undefined, {})).toEqual({ items: [] });
  });

  it("should handle SET_FAVOURITE", () => {
    const initialState = { items: [] };
    const newItems = [{ id: 1, name: "Item A" }];
    const action = { type: SET_FAVOURITE, payload: newItems };
    expect(favouriteReducer(initialState, action)).toEqual({ items: newItems });
  });

  it("should handle ADD_FAVOURITE", () => {
    const initialState = { items: [{ id: 1, name: "Item A" }] };
    const newItem = { id: 2, name: "Item B" };
    const action = { type: ADD_FAVOURITE, payload: newItem };
    const expectedState = { items: [{ id: 1, name: "Item A" }, newItem] };
    expect(favouriteReducer(initialState, action)).toEqual(expectedState);
  });

  it("should handle DELETE_FAVOURITE", () => {
    const initialState = {
      items: [
        { id: 1, name: "Item A" },
        { id: 2, name: "Item B" },
      ],
    };
    const idToDelete = 1;
    const action = { type: DELETE_FAVOURITE, payload: idToDelete };
    const expectedState = { items: [{ id: 2, name: "Item B" }] };
    expect(favouriteReducer(initialState, action)).toEqual(expectedState);
  });
});
