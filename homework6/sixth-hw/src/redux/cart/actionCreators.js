import { getStateFromLocalStorage } from "../../utils/localStorageHelper";
import { SET_CART, ADD_CART, DELETE_CART, CLEAR_CART } from "./actions";

export const fetchCart = () => {
  return async (dispatch) => {
    try {
      const data = getStateFromLocalStorage("cart");
      dispatch(setCart(data));
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
};

export const setCart = (items) => ({
  type: SET_CART,
  payload: items,
});

export const addCart = (items) => ({
  type: ADD_CART,
  payload: items,
});
export const deleteCart = (id) => ({
  type: DELETE_CART,
  payload: id,
});

export const clearCart = (items) => ({ type: CLEAR_CART, payload: items });
