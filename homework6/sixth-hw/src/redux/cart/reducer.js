import { saveStateToLocalStorage } from "../../utils/localStorageHelper";
import { SET_CART, ADD_CART, DELETE_CART, CLEAR_CART } from "./actions";

const initialValue = {
  items: [],
};

const cartReducer = (state = initialValue, action) => {
  switch (action.type) {
    case SET_CART: {
      return { ...state, items: action.payload };
    }

    case ADD_CART: {
      const newItems = [...state.items];
      const item = action.payload;
      newItems.push(item);
      saveStateToLocalStorage("cart", newItems);
      return { ...state, items: newItems };
    }

    case DELETE_CART: {
      const newItems = [...state.items];
      const id = action.payload;
      const index = newItems.findIndex((el) => el.id === id);

      newItems.splice(index, 1);
      saveStateToLocalStorage("cart", newItems);
      return { ...state, items: newItems };
    }
    case CLEAR_CART: {
      const emptyArray = [];

      saveStateToLocalStorage("cart", emptyArray);
      return { ...state, items: emptyArray };
    }
    default:
      return state;
  }
};

export default cartReducer;
