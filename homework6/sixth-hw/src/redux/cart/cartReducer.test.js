import cartReducer from "./reducer";
import { SET_CART, ADD_CART, DELETE_CART, CLEAR_CART } from "./actions";

describe("cartReducer", () => {
  it("should return the initial state", () => {
    expect(cartReducer(undefined, {})).toEqual({ items: [] });
  });

  it("should handle SET_CART", () => {
    const initialState = { items: [] };
    const newItems = [{ id: 1, name: "Product A" }];
    const action = { type: SET_CART, payload: newItems };
    expect(cartReducer(initialState, action)).toEqual({ items: newItems });
  });

  it("should handle ADD_CART", () => {
    const initialState = { items: [{ id: 1, name: "Product A" }] };
    const newItem = { id: 2, name: "Product B" };
    const action = { type: ADD_CART, payload: newItem };
    const expectedState = { items: [{ id: 1, name: "Product A" }, newItem] };
    expect(cartReducer(initialState, action)).toEqual(expectedState);
  });

  it("should handle DELETE_CART", () => {
    const initialState = {
      items: [
        { id: 1, name: "Product A" },
        { id: 2, name: "Product B" },
      ],
    };
    const idToDelete = 1;
    const action = { type: DELETE_CART, payload: idToDelete };
    const expectedState = { items: [{ id: 2, name: "Product B" }] };
    expect(cartReducer(initialState, action)).toEqual(expectedState);
  });

  it("should handle CLEAR_CART", () => {
    const initialState = {
      items: [
        { id: 1, name: "Product A" },
        { id: 2, name: "Product B" },
      ],
    };
    const action = { type: CLEAR_CART };
    const expectedState = { items: [] };
    expect(cartReducer(initialState, action)).toEqual(expectedState);
  });
});
