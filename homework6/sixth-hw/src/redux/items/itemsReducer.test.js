import itemsReducer from "./reducer";
import { SET_ITEM } from "./actions";

describe("itemsReducer", () => {
  it("should return the initial state", () => {
    expect(itemsReducer(undefined, {})).toEqual({ items: [], error: "" });
  });

  it("should handle SET_ITEM", () => {
    const initialState = { items: [], error: "" };
    const newItems = [{ id: 1, name: "Item A" }];
    const action = { type: SET_ITEM, payload: newItems };
    expect(itemsReducer(initialState, action)).toEqual({
      items: newItems,
      error: "",
    });
  });

  it("should not modify state on unknown action", () => {
    const initialState = { items: [], error: "" };
    const unknownAction = { type: "UNKNOWN_ACTION", payload: {} };
    expect(itemsReducer(initialState, unknownAction)).toEqual(initialState);
  });
});
