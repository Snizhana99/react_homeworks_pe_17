import React from "react";
import CardWrapper from "../../components/CardWrapper/CardWrapper";
import { useSelector } from "react-redux";
import CheckoutForm from "../../components/Forms/CheckoutForm";
const CartPage = ({
  addPhoneInCart,
  addPhoneInFavourite,
  deleteFromFavorites,
  deleteFromCart,
}) => {
  const cart = useSelector((state) => state.cart.items);
  return (
    <div>
      <h3>CartPage</h3>

      <div className="cart__wrapper">
        <CheckoutForm></CheckoutForm>
        <CardWrapper
          phones={cart}
          addPhoneInCart={addPhoneInCart}
          addPhoneInFavourite={addPhoneInFavourite}
          deleteFromFavorites={deleteFromFavorites}
          deleteFromCart={deleteFromCart}
        />
      </div>
    </div>
  );
};

export default CartPage;
