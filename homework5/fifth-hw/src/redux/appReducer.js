import { combineReducers } from "redux";
import itemsReducer from "./items/reducer";
import favouriteReducer from "./favourite/reducer";
import cartReducer from "./cart/reducer";
// import modalReducer from "./modal/reducer";

const appReducer = combineReducers({
  items: itemsReducer,
  favourite: favouriteReducer,
  cart: cartReducer,
  //   modal: modalReducer,
});

export default appReducer;
