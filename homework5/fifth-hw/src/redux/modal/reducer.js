import { SET_MODAL, OPEN_MODAL, CLOSE_MODAL } from "./actions";

const initialState = {
  data: [],
  isModalOpen: false,
};

const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MODAL:
      return {
        ...state,
        data: action.payload, // Зберігаємо отримані дані з AJAX у store
      };
    case OPEN_MODAL:
      return {
        ...state,
        isModalOpen: true,
      };
    case CLOSE_MODAL:
      return {
        ...state,
        isModalOpen: false,
      };
    default:
      return state;
  }
};

export default modalReducer;
