import { SET_MODAL, OPEN_MODAL, CLOSE_MODAL } from "./actions";

export const setIsOpenModal = (value) => ({ type: SET_MODAL, payload: value });
export const openModal = () => ({
  type: OPEN_MODAL,
});

export const closeModal = () => ({
  type: CLOSE_MODAL,
});
