import React from "react";
import { useField } from "formik";
import PropTypes from "prop-types";
import styles from "./CustomInput.module.scss";
import { PatternFormat } from "react-number-format";

const CustomInput = ({ type, ...props }) => {
  const [field, meta] = useField(props.name);

  const { error, touched } = meta;

  return (
    <div className={styles.root}>
      <label htmlFor={props.id}>{props.label || field.name}</label>
      {!(props.type === "tel") ? (
        <input {...field} {...props} />
      ) : (
        <PatternFormat
          {...field}
          {...props}
          format="+38 (###) ###-##-##"
          mask="_"
        />
      )}

      {touched && error && <p className={styles.error}>{error}</p>}
    </div>
  );
};

CustomInput.propTypes = {
  type: PropTypes.string,
};

CustomInput.defaultProps = {
  type: "text",
};

export default CustomInput;
