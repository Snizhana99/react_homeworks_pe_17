import React, { memo } from "react";
import IconCart from "../SvgConponents/IconCart";
import StarFilled from "../SvgConponents/StarFilled";
import styles from "./Header.module.scss";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";

const Header = () => {
  const lengthCart = useSelector((state) => state.cart.items.length);
  const lengthFavorite = useSelector((state) => state.favourite.items.length);

  return (
    <>
      <header className={styles.header}>
        <nav className={styles.header__navigation}>
          <ul className={styles.navigation__list}>
            <li className={styles.navigation__item}>
              <NavLink className={styles.header__logo} to="/">
                iShop
              </NavLink>
            </li>
            <li className={styles.navigation__item}>
              <NavLink className={styles.navigation__link} to="/favorites">
                {lengthFavorite} <StarFilled />
              </NavLink>
            </li>
            <li className={styles.navigation__item}>
              <NavLink className={styles.navigation__link} to="/cart">
                <IconCart lengthCart={lengthCart} />
              </NavLink>
            </li>
          </ul>
        </nav>
      </header>
    </>
  );
};

export default memo(Header);
