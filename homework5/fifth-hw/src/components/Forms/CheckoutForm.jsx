import React from "react";
import { Formik, Form } from "formik";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import validationSchema from "./validationSchema";
import CustomInput from "../CustomInput/CustomInput";
import { clearCart } from "../../redux/cart/actionCreators";
import styles from "./CheckoutForm.module.scss";

const CheckoutForm = () => {
  const dispatch = useDispatch();
  const basketItems = useSelector((state) => state.cart.items, shallowEqual);

  const initialValues = {
    "First Name": "",
    "Last Name": "",
    Age: "",
    Address: "",
    "Phone Number": "",
  };

  const onSubmit = (values, actions) => {
    dispatch(clearCart());
    console.log(basketItems);
    console.log(values);
    actions.resetForm();
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
    >
      {(form) => {
        return (
          <Form>
            <CustomInput
              name="First Name"
              placeholder="First Name"
              type="text"
            />
            <CustomInput name="Last Name" placeholder="Last Name" type="text" />
            <CustomInput name="Age" placeholder="Age" type="text" />
            <CustomInput name="Address" placeholder="Address" type="text" />
            <CustomInput
              name="Phone Number"
              placeholder="Phone Number"
              type="tel"
            />

            <button
              className={styles.btnCheckout}
              disabled={!form.isValid}
              type="submit"
            >
              Checkout
            </button>
          </Form>
        );
      }}
    </Formik>
  );
};

export default CheckoutForm;
