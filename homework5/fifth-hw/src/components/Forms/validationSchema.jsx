import * as Yup from "yup";

const validationSchema = Yup.object({
  "First Name": Yup.string().required("Please fill in the field"),
  "Last Name": Yup.string().required("Please fill in the field"),
  Age: Yup.number()
    .required("Please fill in the field")
    .positive("Age must be a positive number")
    .min(18, "Min age is 8")
    .max(80, "Max age is 110"),
  Address: Yup.string().required("Please fill in the field"),
  "Phone Number": Yup.number().required("Please fill in the field"),
});

export default validationSchema;
