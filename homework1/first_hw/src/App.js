import "./App.scss";
import { useState } from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

function App() {
  const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);

  const openFirstModal = () => {
    setIsFirstModalOpen(true);
  };

  const closeFirstModal = () => {
    setIsFirstModalOpen(false);
  };

  const openSecondModal = () => {
    setIsSecondModalOpen(true);
  };

  const closeSecondModal = () => {
    setIsSecondModalOpen(false);
  };

  return (
    <div className="butWrapper">
      <Button
        className="openBtn"
        backgroundColor="rgb(76, 0, 255)"
        onClick={openFirstModal}
        text="Open first modal"
      ></Button>
      <Button
        className="openBtn"
        backgroundColor="rgb(255, 145, 0)"
        onClick={openSecondModal}
        text="Open second modal"
      >
        Open second modal
      </Button>
      {isFirstModalOpen && (
        <Modal
          modalIsOpen={isFirstModalOpen}
          setModalIsOpen={setIsFirstModalOpen}
          closeModal={closeFirstModal}
          closeBtn={true}
          title="Do you want to delete this file?"
          text={
            "Once you delete this file, it won't be possible to undo this action."
          }
          text2={"Are you sure you want to delete it?"}
          actions={
            <>
              <Button className="butnOK" backgroundColor="#b43725" text="Ok">
                Ok
              </Button>
              <Button
                className="butnCancel"
                backgroundColor="#b43725"
                onClick={closeFirstModal}
                text="Cancel"
              >
                Cancel
              </Button>
            </>
          }
        />
      )}
      {isSecondModalOpen && (
        <Modal
          modalIsOpen={isSecondModalOpen}
          setModalIsOpen={setIsSecondModalOpen}
          closeModal={closeSecondModal}
          closeBtn={true}
          title="Another modal. oh no"
          text="It should be anouther modal, but it's not"
          actions={
            <>
              <Button className="butnOK" backgroundColor="#b43725" text="Ok">
                Ok
              </Button>
              <Button
                className="butnCancel"
                backgroundColor="#b43725"
                onClick={closeSecondModal}
                text="Cancel"
              >
                Cancel
              </Button>
            </>
          }
        />
      )}
    </div>
  );
}

export default App;
