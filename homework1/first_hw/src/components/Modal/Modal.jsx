import React from "react";
import styles from './Modal.module.scss';


function Modal ({id, closeBtn, actions, text, text2, title, closeModal }) {


    return <>

    <div className={styles.modal} onClick={closeModal} >

        <div className={styles.modal__content} onClick={(e) => e.stopPropagation()}>
          <header className={styles.content__header}>
            <h1 id= {id} className={styles.header__title}>{title}</h1>
            {closeBtn && (<p className={styles.header__closeCross} onClick={closeModal}>✕</p>)}
          </header>
        <main className={styles.content__main}>
			  <p id= {id} className={styles.main__text}>{text} <br /> {text2}</p>
			  <div className={styles.main__butnWrap}>{actions}</div>

		  </main>
      </div>
    </div>

    </>
        
}

export default Modal;
